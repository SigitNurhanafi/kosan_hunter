<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/search/', 'HomeController@search');

Auth::routes();

Route::get('/admin/', function(){
  return redirect('/dashboard');
});

Route::get('/member/', function(){
  return redirect('/dashboard');
});

Route::get('/home/', function(){
  return redirect('/dashboard');
});

Route::get('/dashboard', 'HomeController@index');
Route::get('/explore', 'SearchController@filter');
Route::get('/explore/{slug_url}', 'HomeController@detail');


Route::group(['prefix'=>'admin', 'middleware'=>['role:admin'] ], function () {
  Route::get('/admin/', 'PengurusWebController@index');
  Route::match(['get', 'post'], '/kosan-baru/' , 'PengurusKosanController@create')->name('admin.kosan-baru');
  Route::match(['get', 'post'], '/list-kosan/' , 'PengurusKosanController@showAllKosan')->name('admin.list-kosan');

});

Route::group(['prefix'=>'member', 'middleware'=>['role:member'] ], function () {
  Route::get('/member', 'PengurusKosanController@indexKosan');
  Route::match(['get', 'post'], '/kosan-baru/' , 'PengurusKosanController@createKosan')->name('member.kosan-baru');
  Route::match(['get', 'post'], '/list-kosan/' , 'PengurusKosanController@showAllKosan')->name('member.list-kosan');

  Route::get('/list-kosan/{slug}/edit/' , 'PengurusKosanController@editKosan')->name('member.view-edit-kosan');
  Route::post('/list-kosan/{slug}/edit/' , 'PengurusKosanController@updateKosan')->name('member.update-kosan');

  Route::get('/list-kosan/{slug}/delete/' , 'PengurusKosanController@destroyKosan')->name('member.delete-kosan');

  Route::get('/profile/' , 'PengurusKosanController@editUser')->name('member.edit-user');
  Route::post('/profile/' , 'PengurusKosanController@updateUser')->name('member.update-user');

  Route::match(['get', 'post'], '/update-password/', 'PengurusKosanController@updatePassword')->name('member.update-password');
  Route::match(['get', 'post'], '/contact-admin/', 'ContactController@sendAdmin')->name('member.contact-admin');

});
