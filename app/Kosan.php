<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kosan extends Model
{

  protected $fillable = [
      'name', 'foto_kosan',
      'harga_kosan','tempobayar',
      'jenis_penghuni','nama_kontak',
      'nomer_kontak','alamat','foto_kosan',
      'deskripsi_kosan','slug_url','user_id',
  ];

  // protected $guard = [
  //   'id' , 'created_at' , 'updated_at',
  // ];


  public function user()
  {
    return $this->belongsTo('App\User');
  }
}
