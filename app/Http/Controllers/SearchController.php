<?php

namespace App\Http\Controllers;


use App\Kosan;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function filter(Request $request ,Kosan $kosan)
    {
      $kosan_list     = Kosan::orderBy('name','asc')->Paginate(3);
      $jumlah_kosan   = Kosan::count();
      $tempo_bayar    = $request->tempo_bayar;
      $tipe_kosan     = $request->tipe_kost;
      $rentang_harga  = $request->rentang_harga;
      $search         = $request->search;



      $kosan = $kosan->newQuery();

       //mencari nama kosan yang mirip dengan
      if ($request->has('search')) {
        $kosan->where('name', 'LIKE', '%'.$search.'%' )->orWhere('alamat', 'LIKE', '%'.$search.'%' );
      }
       //mencari tempo bayar
      if ($request->has('tempo_bayar')) {
        $kosan->where('tempobayar', '=', $tempo_bayar );
      }

      //mencari tipe kosan
      if ( $request->has('tipe_kost')) {
          $kosan->where('jenis_penghuni', '=', $tipe_kosan );
      }

      if ( isset($rentang_harga) ) {
        switch ($rentang_harga) {
          case 'low':
            $kosan->where('harga_kosan', '<', 500000 );
            break;
          case 'medium':
            $kosan->whereBetween('harga_kosan', [500000, 1000000]);
            break;
          case 'high':
            $kosan->where('harga_kosan', '>', 1000000 );
            break;
        }
      }

      // return $coba;
      // return dd($coba);
      $kosan_list = $kosan->get();

      // return dd($kosan_list);

     return view('explore.index_explore',compact('kosan_list','jumlah_kosan','tipe_kosan','tempo_bayar','rentang_harga','search'));

      //return dd($request->all());
    }
}
