<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Laratrust\LaratrustFacade as Laratrust;
use App\Kosan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Laratrust::hasRole('admin')) return $this->adminDashboard();
        if(Laratrust::hasRole('member')) return $this->memberDashboard();
        if(Laratrust::hasRole('suspend')) return $this->memberSuspend($request);

        return redirect('/login');;
    }

    public function explore(Request $request)
    {
      $kosan_list     = Kosan::orderBy('name','asc')->Paginate(3);
      $jumlah_kosan   = Kosan::count();
      $tempo_bayar    = $request->tempo_bayar;
      $tipe_kosan     = $request->tipe_kost;
      $rentang_harga  = $request->rentang_harga;
      $search         = $request->search;

    //  return view('explore.index_explore',compact('kosan_list','jumlah_kosan','tipe_kosan','tempo_bayar','rentang_harga','search'));
      return dd($tipe_kosan);
    }

    public function detail($slug_url)
    {
      if( !(Session::get('slug_url') == $slug_url)){
        Kosan::where('slug_url',$slug_url)->increment('view_count');
        // dd('added count');
        Session::put('slug_url', $slug_url);
      }

        $kosan  = Kosan::where('slug_url',$slug_url)->first();

        return view('explore.detail',compact('kosan'));

    }

    protected function adminDashboard(){
      if(Laratrust::hasRole('suspend')) return $this->memberSuspend();
      return view('DashboardPengurusWeb');
    }

    protected function memberDashboard(){
      if(Laratrust::hasRole('suspend')) return $this->memberSuspend();
      return view('DashboardPengurusKosan');
    }

    protected function memberSuspend( $request)
    {
      $request->session()->flush();
      return view('suspend');
    }

}
