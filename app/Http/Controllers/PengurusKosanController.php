<?php

namespace App\Http\Controllers;

use App\Kosan;
use App\User;

use Illuminate\Http\Request;
use Laratrust\LaratrustFacade as Laratrust;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

use File;
use Image;
use Auth;

class PengurusKosanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexKosan()
    {
      return view('DashboardPengurusKosan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createKosan(Request $request)
    {

      if ($request->isMethod('post')) {

        $this->validate($request, [
          'nama_kosan'=>  'required|min:3',
          'harga_kosan'=> 'required|max:12',
          'tempobayar'=>  'required',
          'penghuni'=>    'required',
          'nama_pemilik'=>'required|min:3',
          'nomer_telpon'=>'required|max:12',
          'deskripsi'=>   'required|min:120',
          'alamat'=>      'required|min:25',
          'foto_kosan'=>  'required|size:3500|mimes:jpeg,jpg,png',
        ]);

        $image = $request->file('foto_kosan');
        $filename_image = time() .'_'. $image->getClientOriginalName();
        Image::make($image)->resize(310, 300)->save(public_path('/img/kosan/' . $filename_image));

        $slug_url = str_slug($request->nama_kosan, '-');
        if(Kosan::where('slug_url', $slug_url)->first() != null )
          $slug_url = time() . '-' . $slug_url;

        $kosan = Kosan::create([
          'name' =>             $request->nama_kosan,
          'harga_kosan' =>      $request->harga_kosan,
          'tempobayar' =>       $request->tempobayar,
          'jenis_penghuni' =>   $request->penghuni,
          'nama_kontak' =>      $request->nama_pemilik,
          'nomer_kontak' =>     $request->nomer_telpon,
          'deskripsi_kosan' =>  $request->deskripsi,
          'alamat' =>           $request->alamat,
          'slug_url' =>         $slug_url,
          'foto_kosan' =>       $filename_image,
          'user_id'=>           Auth::user()->id,
        ]);

        return redirect('/list-kosan');

      }

        return view('dashboard.member.kosanBaru');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    public function showAllKosan()
    {

      $kosans = Kosan::where('user_id', '=', Auth::user()->id)->get();

      return view('dashboard.member.kosanList', compact('kosans') );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function editKosan($slug)
    {
        $kosan = Kosan::where('slug_url', $slug)->first();

        if ( empty($kosan) or is_null($slug) ) {
          abort(404);
        }

        return view('dashboard.member.kosanEdit', compact('kosan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function updateKosan(Request $request, $slug)
    {

      $this->validate($request, [
        'nama_kosan'    =>  'required|min:3',
        'harga_kosan'   =>  'required|max:12',
        'tempobayar'    =>  'required',
        'penghuni'      =>  'required',
        'nama_pemilik'  =>  'required|min:3',
        'nomer_telpon'  =>  'required|max:12',
      //  'deskripsi'     =>  'required|min:100|max:191',
      //  'alamat'        =>  'required|min:25|max:191',
      ]);

      DB::table('kosans')->where('slug_url',$slug)->update([
        'name'            => $request->nama_kosan,
        'harga_kosan'     => $request->harga_kosan,
        'tempobayar'      => $request->tempobayar,
        'jenis_penghuni'  => $request->penghuni,
        'nama_kontak'     => $request->nama_pemilik,
        'nomer_kontak'    => $request->nomer_telpon,
        'alamat'          => $request->alamat,
        'deskripsi_kosan' => $request->deskripsi,
      ]);

      //dd( Kosan::where('slug_url', $slug)->get() );
      $kosan = Kosan::where('slug_url', $slug)->first();

      if ( ($request->file('foto_kosan')) !== NULL )
      {
        File::Delete('img/kosan/'.$kosan->foto_kosan);

        $image = $request->file('foto_kosan');
        $filename_image = time() .'_'. $image->getClientOriginalName();
        Image::make($image)->resize(310, 300)->save(public_path('/img/kosan/' . $filename_image));

        Kosan::where('slug_url', $slug)->update(['foto_kosan' => $filename_image]);

      }

      return redirect(route('member.list-kosan',[ 'slug' => $kosan->slug_url ]))->with('msg', 'Data "'.$kosan->name.'" berhasil di perbaharui');

    }

    public function editUser()
    {

      $user  = User::where('id', Auth::user()->id)->first();
      return view('dashboard.member.settingAkun', compact('user'));

    }

    public function updateUser(Request $request )
    {

      $user_id  = User::findOrFail(Auth::user()->id);
      $user_id->first_name  = $request->nama_depan;
      $user_id->last_name   = $request->nama_belakang;
      $user_id->email       = $request->email;
      $user_id->save();

      //return redirect(route('member.update-user'));;
      return redirect(route('member.edit-user'))->with('msg', 'profile anda berhasil di perbaharui');
    }

    public function updatePassword(Request $request )
    {
      if ($request->isMethod('post')){

        $this->validate($request, [
          'curent_password' =>  'required|current_password_match|min:6',
          'new_password'    =>  'required|min:6',
          // 'confirm_password' => 'required|min:6',
        ]);

        request()->user()->fill([
        'password' => Hash::make(request()->input('new_password'))
        ])->save();

        return redirect(route('member.update-password'))->with('msg', 'berhasil di update');
      }

      return view('dashboard.member.updatePassword');
    }

    public function destroyKosan($slug)
    {
      DB::table('kosans')->where('slug_url', '=', $slug)->delete();
      return redirect(route('member.list-kosan'))->with('msg', 'berhasil di delete');
    }

}
