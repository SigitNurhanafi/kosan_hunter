<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ContactAdmin;
use Auth;

class ContactController extends Controller
{

  public function sendAdmin(Request $request)
  {
    if ($request->isMethod('post'))
    {
      $this->validate($request, [
        'judul_pesan' =>  'required|min:3',
        'isi_pesan'   =>  'required|min:12',
      ]);

      ContactAdmin::create([
        'judul_pesan'=>$request->judul_pesan,
        'isi_pesan'=>$request->isi_pesan,
        'email'=>Auth::user()->email,
        'user_id' => Auth::user()->id,
      ]);

      return redirect(route('member.contact-admin'))->with('msg', 'pesan berhasil dikirim');
    }

    return view('dashboard.member.AdminContact');
  }

  public function sendMember(Request $request)
  {
    dd($request);
  }

}
