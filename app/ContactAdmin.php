<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactAdmin extends Model
{
    protected $fillable = [
      'judul_pesan','isi_pesan', 'email','user_id',
    ];
}
