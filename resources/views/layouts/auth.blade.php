<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>

      Login
    </title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="icon"
      type="image/png"
      href="asset/img/favicon.png">
    <style>
    *{padding: 0px;margin: 0px;}
		#bg {
			position:fixed;
			top:-50%;
			left:-50%;
			width:200%;
			height:200%;
		}
		#bg img {
			position:absolfute;
			top:0;
			left:0;
			right:0;
			bottom:0;
			margin:auto;
			min-width:50%;
			min-height:50%;
		}
		#page-wrap {
       position: relative;
       z-index: 2;
       width: 400px;
       margin: 50px auto;
       padding: 40px 20px 45px 20px;
       background: white;
       opacity: 0.7;
       -moz-box-shadow: 0 0 20px black;
       -webkit-box-shadow: 0 0 20px black;
       box-shadow: 0 0 20px black;
    }
    .container{
      font-size: 20 px;
      font-family: 'Raleway', sans-serif;
      font-weight: bold;
    }
    .container button{
      background-color: #00BCD4;
      color: white;
      padding: 14px 20px;
      margin: 8px 0;
      border: none;
      cursor: pointer;
      width: 100%;
    }
    input[type=text], input[type=password],input[type=email]{
      width: 100%;
      padding: 12px 20px;
      margin: 8px 0;
      display: inline-block;
      border: 1px solid #00BCD4;
      box-sizing: border-box;
      font-size: 20px;
    }
	</style>
  </head>
  <body >

    <div id="page-wrap">

      {{-- <center style="margin-bottom:20px;">
        <img src="asset/img/logo_restaura.png" alt="logo sundanese restauran">
      </center> --}}
      @yield('content')
	  </div>
    <div id="bg">
      <img src="asset/img/login-bg-2.jpeg" alt="">
    </div>
  </body>
    <script type="text/javascript">
      window.onload = function() {
      document.getElementById("uname").focus();
      };
    </script>
</html>
