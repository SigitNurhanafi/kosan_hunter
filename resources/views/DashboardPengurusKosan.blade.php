@extends('layouts.dashboard')
@section('menu-navigation')
        <h2>Menu</h2>
        <ul style="list-style: none">

          <li class="Link">
            <a href="/explore">
              <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
              <span>Explore</span>
            </a>
          </li>

          <li class="Link">
            <a data-toggle="collapse" href="#DataKosan" aria-controls="DataKosan">
              <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
              <span>Data kosan</span>
            </a>
            <ul class="collapse" id="DataKosan">
              <li class="Link">
                {{-- <span class="glyphicon glyphicon-plus"></span> --}}
                <span><a href="/member/kosan-baru">Kosan Baru</a></span>
              </li>
              <li class="Link">
                {{-- <span class="glyphicon glyphicon-th-list"></span> --}}
                <span><a href="/member/list-kosan">List Kosan</a></span>
              </li>
            </ul>
          </li>

          <li class="Link">
            <a data-toggle="collapse" href="#SettingAccount" aria-controls="SettingAccount">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              <span>Setting account</span>
            </a>
            <ul class="collapse" id="SettingAccount" >
              <li class="Link">
                {{-- <span class="glyphicon glyphicon-user"></span> --}}
                <span><a href="{{route('member.update-user')}}">Profile Saya</a></span>
              </li>
              <li class="Link">
                {{-- <span class="glyphicon glyphicon-lock"></span> --}}
                <span><a href="{{route('member.update-password')}}">Ganti Password</a></span>
              </li>
            </ul>
          </li>

          <li class="Link">
            <a href="{{route('member.contact-admin')}}">
              <span class="glyphicon glyphicon-envelope"></span>
              <span>Contact Admin</span>
            </a>
          </li>

        </ul>
@endsection
