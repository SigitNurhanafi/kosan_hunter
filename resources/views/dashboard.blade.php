<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Dashboard</title>

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- bootstrap -->
      <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <!-- Built CSS -->
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">

    <style media="screen">
      .test-css{
        border:solid 1px #4BA5C6;
      }
    </style>

  </head>
  <body>
  <div class="container-fluid display-table">
  <div class="row display-table-row">

  {{-- menu navigation --}}
  <div class="col-md-2 display-table-cell valign-top" id="side-menu" >
    <div class="row nav">
      @yield('menu-navigation')
    </div>
  </div>

  <div class="col-md-10 display-table-cell valign-top" id="dashbord-content">
    <div class="row">

        <header id="nav-header" class="clearfix">
          <div class="col-md-5">
          </div>
          <div class="col-md-7">
            <ul class="pull-right">
              <li>Hi {{ Auth::user()->first_name }}, selamat datang di Dashboard </li>
              <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                  <span class="glyphicon glyphicon-log-out" aria-hidden="true">
                  </span>Log out
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> {{ csrf_field() }}</form>
              </li>
            </ul>
          </div>
        </header>
    </div>
    @yield('content')
    <div class="row">
        <footer id="dashbord-footer" class="clearfix">
          <div class="pull-left">
            <b style="padding:5px;">Copyright</b> &copy; 2017 - ATOL-x
          </div>
        </footer>
      </div>
    </div>
  </div>
  </div>
  </body>
</html>
