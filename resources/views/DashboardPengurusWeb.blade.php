@extends('layouts.dashboard')
@section('menu-navigation')
  <h2>Menu</h2>
  <ul style="list-style: none">

    <li class="Link">
      <a href="/explore">
        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
        <span>Dashbord</span>
      </a>
    </li>

    <li class="Link">
      <a data-toggle="collapse" href="#DataKosan" aria-controls="DataKosan">
        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
        <span>Data kosan</span>
      </a>
      <ul class="collapse" id="DataKosan">
        <li class="Link">
          {{-- <span class="glyphicon glyphicon-plus"></span> --}}
          <span><a href="/kosan-baru">Kosan Baru</a></span>
        </li>
        <li class="Link">
          {{-- <span class="glyphicon glyphicon-th-list"></span> --}}
          <span><a href="/list-kosan">List Kosan</a></span>
        </li>

      </ul>
    </li>

    <li>
      <li class="Link">
        <a data-toggle="collapse" href="#DataUser" aria-controls="DataUser">
          <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
          <span>Data User Website</span>
        </a>
        <ul class="collapse" id="DataUser">
          <li class="Link">
            {{-- <span class="glyphicon glyphicon-plus"></span> --}}
            <span><a href="/user-baru">User Baru</a></span>
          </li>
          <li class="Link">
            {{-- <span class="glyphicon glyphicon-th-list"></span> --}}
            <span><a href="/list-kosan">List User</a></span>
          </li>
          <li class="Link">
            <span><a href="/approve-user">Approve User</a></span>
          </li>
        </ul>
      </li>
    </li>

    <li class="Link">
      <a data-toggle="collapse" href="#SettingAccount" aria-controls="SettingAccount">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        <span>Setting account</span>
      </a>
      <ul class="collapse" id="SettingAccount" >
        <li class="Link">
          {{-- <span class="glyphicon glyphicon-user"></span> --}}
          <span><a href="#">Profile Saya</a></span>
        </li>
        <li class="Link">
          {{-- <span class="glyphicon glyphicon-lock"></span> --}}
          <span><a href="#">Ganti Password</a></span>
        </li>
      </ul>
    </li>
  </ul>
@endsection
