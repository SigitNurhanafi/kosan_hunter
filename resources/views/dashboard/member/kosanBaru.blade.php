@extends('DashboardPengurusKosan')


@section('content')
<ins><h2>Informasi Kosan </h2></ins><br>
<div class="col-md-8 col-md-offset-1">

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li> {{$error}} </li>
        @endforeach
      </ul>
    </div>

  @endif

  <form class="form-horizontal" method="post" action="{{ route('member.kosan-baru') }} " enctype="multipart/form-data">
      {{ csrf_field() }}

    <div class="form-group">
      <label class="control-label col-sm-2" for="nama_kosan">Nama Kosan</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="nama_kosan" value="{{old('nama_kosan')}}" placeholder="ex : kosan Indah Pemata">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="harga">Harga</label>
      <div class="col-sm-10">
        <input type="text" class="form-control"  name="harga_kosan" value="{{old('harga_kosan')}}" placeholder="ex : 250000">
        <label class="radio-inline"><input type="radio" name="tempobayar" value="Perbulan">Perbulan</label>
        <label class="radio-inline"><input type="radio" name="tempobayar" value="pertahun">Pertahun</label>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="penghuni">Penghuni</label>
      <div class="col-sm-10">
        <select class="form-control" id="sel1" name="penghuni" selected="Pria">
          <option value="Wanita/Pria" >Wanita/Pria</option>
          <option value="Pria" >Pria</option>
          <option value="Wanita" >Wanita</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="nama_pemilik">Nama Pemilik</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="nama_pemilik" placeholder="ex : Pak Tego">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="nomer_telpon">Nomer Telphone</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="nomer_telpon" placeholder="ex : 08900000000">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="alamat">Alamat</label>
      <div class="col-sm-10">
        <textarea class="form-control" rows="4" name="alamat" placeholder="ex : jalan Plered no 13 , antapani, kota bandung"></textarea>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="deskripsi">Deskripsi</label>
      <div class="col-sm-10">
        <textarea class="form-control" rows="4" name="deskripsi" placeholder="ex : kawasan hunian dekat dengan kampus UI"></textarea>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2"  for="foto_kosan">Foto</label>
      <div class="col-sm-10">
        <input type="file" name="foto_kosan" id="exampleInputFile">
        <p class="help-block">Maksimal ukuran file gambar 3 Mb</p>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
      </div>
    </div>

  </form>
</div>
@endsection
