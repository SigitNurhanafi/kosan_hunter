@extends('DashboardPengurusKosan')

@section('content')
<ins><h2>Edit Informasi Kosan </h2></ins><br>
<div class="col-md-8 col-md-offset-1">

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li> {{$error}} </li>
        @endforeach
      </ul>
    </div>
  @endif

  <form class="form-horizontal" method="post" action="{{ route('member.update-kosan' ,$kosan->slug_url ) }} " enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
      <label class="control-label col-sm-2" for="nama_kosan">Nama Kosan</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="nama_kosan"  value="@if(old('nama_kosan') == NULL){{$kosan->name}}@else{{old('nama_kosan')}}@endif" placeholder="ex : kosan Indah Pemata">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="harga">Harga</label>
      <div class="col-sm-10">
        <input type="text" class="form-control"  name="harga_kosan" value="@if(old('harga_kosan') == NULL){{$kosan->harga_kosan}}@else{{old('harga_kosan')}}@endif" placeholder="ex : 250000">

        <label class="radio-inline"><input type="radio" name="tempobayar" value="Perbulan" @if( ( $kosan->tempobayar == 'Perbulan') or ( old('harga_kosan') == 'Perbulan') ) checked ="checked" @endif>Perbulan</label>
        <label class="radio-inline"><input type="radio" name="tempobayar" value="Pertahun" @if( ( $kosan->tempobayar == 'Pertahun') or ( old('harga_kosan') == 'Pertahun') ) checked ="checked" @endif >Pertahun</label>

      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="penghuni">Penghuni</label>
      <div class="col-sm-10">
        <select class="form-control" id="sel1" name="penghuni">
          <option value="Wanita/Pria"  @if( ( $kosan->jenis_penghuni == 'Wanita/Pria') or ( old('penghuni') == 'Wanita/Pria') ) selected @endif >Wanita/Pria</option>
          <option value="Pria" @if( ( $kosan->jenis_penghuni == 'Pria') or ( old('penghuni') == 'Pria') ) selected @endif >Pria</option>
          <option value="Wanita" @if( ( $kosan->jenis_penghuni == 'Wanita') or ( old('penghuni') == 'Wanita') ) selected @endif >Wanita</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="nama_pemilik">Nama Pemilik</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="nama_pemilik" value="@if(old('nama_pemilik') == NULL){{$kosan->nama_kontak}}@else{{old('nama_pemilik')}}@endif" placeholder="ex : Pak Tego">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="nomer_telpon">Nomer Telphone</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="nomer_telpon" value="@if(old('nomer_telpon') == NULL){{$kosan->nomer_kontak}}@else{{old('nomer_telpon')}}@endif" placeholder="ex : 08900000000">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="alamat">Alamat</label>
      <div class="col-sm-10">
        <textarea class="form-control" rows="4" name="alamat" placeholder="ex : jalan Plered no 13 , antapani, kota bandung">@if(old('alamat') == NULL){{$kosan->alamat}}@else{{old('alamat')}}@endif</textarea>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="deskripsi">Deskripsi</label>
      <div class="col-sm-10">
        <textarea class="form-control" rows="4" name="deskripsi" placeholder="ex : kawasan hunian dekat dengan kampus UI">@if(old('deskripsi') == NULL){{$kosan->deskripsi_kosan}}@else{{old('deskripsi')}}@endif</textarea>
      </div>
    </div>
    <center><img src="{{asset('/img/kosan/'.$kosan->foto_kosan)}}" alt="foto {{$kosan->nama_kontak}}" width="300" height="300" ></center>

    <div class="form-group" style="padding-top:15px;">
      <label class="control-label col-sm-2"  for="foto_kosan">Foto</label>
      <div class="col-sm-10">
        <input type="file" name="foto_kosan" id="exampleInputFile">
        <p class="help-block">Maksimal ukuran file gambar 3 Mb</p>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
      </div>
    </div>

  </form>
</div>
@endsection
