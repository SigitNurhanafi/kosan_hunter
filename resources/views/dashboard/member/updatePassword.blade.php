@extends('DashboardPengurusKosan')

@section('content')



<ins><h2>Update Password</h2></ins><br>

@if (session('msg'))
    <div class="alert alert-success" style="margin-top:10px;">
        {{ session('msg') }}
    </div>
@endif

@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li> {{$error}} </li>
      @endforeach
    </ul>
  </div>

@endif
<div class="col-md-3">
    <form action="{{route('member.update-password')}}" method="post">
      {{csrf_field()}}
      <div class="form-group">
        <label for="">Password Lama</label>
        <input type="password" class="form-control" id="" name="curent_password" placeholder=" ">

        <label for="">Password Baru</label>
        <input type="password" class="form-control" id="" name="new_password" placeholder="" >

        {{-- <label for="">Ulangi Password Baru</label>
        <input type="password" class="form-control" id="" name="confirm_password" placeholder="" > --}}

        <button type="submit" class="btn btn-info pull-right" style="margin-top:10px;">save</button>

      </div>
    </form>
</div>
@endsection
