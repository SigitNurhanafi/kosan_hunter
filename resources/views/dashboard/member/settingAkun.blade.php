@extends('DashboardPengurusKosan')

@section('content')

  @if (session('msg'))
      <div class="alert alert-success" style="margin-top:10px;">
          {{ session('msg') }}
      </div>
  @endif

  <h1>hi ,{{ $user->first_name }} {{ $user->last_name }}</h1>
<div class="col-md-3">
    <form action="{{route('member.update-password')}}" method="post">
      {{csrf_field()}}
      <div class="form-group">
        <label for="">Nama depan</label>
        <input type="text" class="form-control" id="" name="nama_depan" placeholder="" value="{{ $user->first_name }}">
        <label for="">Nama depan</label>
        <input type="text" class="form-control" id="" name="nama_belakang" placeholder="" value="{{ $user->last_name }}">
        <label for="">Email anda</label>
        <input type="email" class="form-control" id="" name="email" placeholder="" disabled value="{{ $user->email }}">
        <button type="submit" name="save" class="btn btn-info pull-right" style="margin-top:10px;">save</button>
      </div>
    </form>
</div>
@endsection
