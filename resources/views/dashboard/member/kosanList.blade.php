@extends('DashboardPengurusKosan')

@section('content')
  @if (session('msg'))
    <div class="alert alert-success">
        {{ session('msg') }}
    </div>
  @endif

  <div class="row">
  <form class="navbar-form navbar-left">
    <div class="form-group">
      <input type="text"  name="cari" class="form-control" placeholder="Search">
    </div>
    <button type="submit" class="btn btn-info">Cari</button>
  </form>
  </div>

  <div class="row">
    @if ( count($kosans) == 0 )
        <center><p>Anda tidak memiliki data kosan<p></center>
    @else

        @foreach ($kosans as $kosan)
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
              <img src="{!!  asset('/img/kosan/'.$kosan->foto_kosan); !!}" alt="gambar kosan">
              <div class="caption">
                <h3>{{ $kosan->name }}</h3>
                <p>
                  <a href="/member/list-kosan/{{ $kosan->slug_url }}/edit" class="btn btn-warning" role="button">Edit</a>
                  <a href="/member/list-kosan/{{ $kosan->slug_url }}/delete" class="btn btn-danger" role="button">Delete</a>

                </p>
              </div>
            </div>
          </div>
        @endforeach
    @endif
  </div>

@endsection
