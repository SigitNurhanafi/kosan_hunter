@extends('DashboardPengurusKosan')
@section('content')
  <ins><h2>Contact Admin </h2></ins><br>
  @if (session('msg'))
    <div class="alert alert-success">
        {{ session('msg') }}
    </div>
  @endif

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li> {{$error}} </li>
        @endforeach
      </ul>
    </div>
  @endif
  <div class="col-md-10" style="padding-left:15%;">
      <form action="" method="post">
        {{csrf_field()}}
        <div class="form-group">
          <label for="">Subjek</label>
          <input type="text" class="form-control" id="" name="judul_pesan" placeholder="" >
          {{-- <label for="">email</label>
          <input type="text" class="form-control" id="" name="email" value="{{$email}}" disabled> --}}
          <label for="">Isi Pesan</label>
          <textarea type="text" rows="5" class="form-control" id="" name="isi_pesan" placeholder="" ></textarea>

          <button type="submit"class="btn btn-info pull-right" style="margin-top:10px;">kirim</button>

        </div>
      </form>
  </div>
@endsection
