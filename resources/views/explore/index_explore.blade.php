@extends('layouts.exploreKosan')
@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <form>
                    {{-- <input type="text" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                      <button class="btn-default" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                      </button>
                    </div> --}}

                    <div class="input-group" style="width:100%;">
                      <input type="text" placeholder="ketik nama atau alamat kosan disini" class="form-control" value="{{ isset($search) ? $search:'' }}" name="search">
                      <div class="input-group-btn">
                        <button type="submit" class="btn" style="color:black !important;">Cari</button>
                      </div>
                    </div>

                  <br>
                  <div class="form-group">
                      <label name="tipe_kost"> Tipe Kost </label>
                      <div class="checkbox">

                        <label>
                          <input type="radio" name="tipe_kost" value="Pria"{{ ( $tipe_kosan == 'Pria' ) ? 'checked':'' }} >Pria
                        </label>
                      </div>

                      <div class="checkbox">
                        <label>
                          <input type="radio" name="tipe_kost"  value="Wanita" {{ ( $tipe_kosan == 'Wanita' ) ? 'checked':'' }} >Wanita
                        </label>
                      </div>

                      <div class="checkbox">
                        <label>
                          <input type="radio" name="tipe_kost"  value="Pria/Wanita"{{ ( $tipe_kosan == 'Pria/Wanita' ) ? 'checked':'' }}>Campur
                        </label>
                      </div>

                    </div>

                    <div class="form-group">
                      <label for="tempo_bayar">Tempo Bayar</label>
                      <select class="form-control" name="tempo_bayar">


                        <option value="Pertahun" {{ ( $tempo_bayar === 'Pertahun' ) ? 'selected':'' }}>Pertahun</option>
                        <option value="Perbulan" {{ ( $tempo_bayar === 'Perbulan' ) ? 'selected':'' }}>Perbulan</option>
                      </select>
                    </div>

                      <div class="form-group">
                          <label for="rentang_harga">Rentang Harga</label>
                          <select class="form-control" name="rentang_harga" >
                            <option value="low" {{ ( $rentang_harga === 'low' ) ?  'selected':'' }} > &lt Rp500.000/Bln</option>
                            <option value="medium" {{ ( $rentang_harga === 'medium' ) ?  'selected':'' }} > Rp500.000-Rp1.000.000/Bln</option>
                            <option value="high" {{ ( $rentang_harga === 'high' ) ?  'radio':'' }} > &gt Rp1.000.000/Bln</option>
                          </select>
                      </div>
                    </form>
            </div>

            <div class="col-md-9">

                <div class="row">
                    <hr>
                    @if(!empty($kosan_list))
                      @foreach ($kosan_list as $kosan)
                      <div class="col-sm-4 col-lg-4 col-md-4">
                          <div class="thumbnail">
                              <img src="{!!  asset('/img/kosan/'.$kosan->foto_kosan); !!}" alt="">
                              <div class="caption">
                                  <h4 class="pull-right">{{$kosan->harga_kosan}}</h4>
                                  <h4><a href="{{'explore/'.$kosan->slug_url}}">{{$kosan->name}}</a>
                                  </h4>
                                  <p>{{$kosan->deskripsi_kosan}}</p>

                                  <div class="ratings">
                                      <p>
                                        View : {{ $kosan->view_count }}  <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                      </p>
                                  </div>

                              </div>

                          </div>
                      </div>
                    @endforeach

                    {{ old('') }}

                  @else
                    <p>Tidak ada ada</p>
                  @endif
                </div>

                <div class="paging">
                  {{-- {{ $kosan_list->links() }} --}}
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->
@endsection
