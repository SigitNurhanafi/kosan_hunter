@extends('layouts.exploreKosan')
@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="row">

          <div class="col-md-12">
              <div class="thumbnail">
                  <img class="img-responsive" src="{{ asset('/img/kosan/'.$kosan->foto_kosan) }}" alt="">
                  <div class="caption-full">
                  
                       <h1>{{$kosan->name}} </h1>
                       <h4>Rp {{$kosan->harga_kosan}} / {{$kosan->tempobayar}} </h4>
                       <h5><ins><b>Penghuni</b></ins> : {{$kosan->jenis_penghuni}}</h5>
                       <h5><ins><b>kontak</b></ins> :</h5>
                       <p>
                         {{$kosan->nama_kontak}}<br>
                         {{$kosan->nomer_kontak}}
                       </p>

                       <h5><ins><b>alamat</b></ins> :</h5>
                       <p>{{$kosan->alamat}}</p>

                      <h5><ins><b>desktipsi</b></ins> :</h5>
                       <p>{{$kosan->deskripsi_kosan}}</p>
                  </div>

              </div>
          </div>

        </div>

    </div>
    <!-- /.container -->
    @endsection
