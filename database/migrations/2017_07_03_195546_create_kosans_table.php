<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKosansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kosans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('foto_kosan');
            $table->integer('harga_kosan');
            $table->string('tempobayar');
            $table->string('jenis_penghuni');
            $table->string('alamat');
            $table->string('nomer_kontak');
            $table->string('nama_kontak');
            $table->string('deskripsi_kosan');
            $table->string('slug_url');
            $table->integer('view_count');
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kosans');
    }
}
