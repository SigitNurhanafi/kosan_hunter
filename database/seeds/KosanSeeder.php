<?php

use Illuminate\Database\Seeder;
use App\Kosan;

class KosanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataKosan = new Kosan();
        $dataKosan->name = 'kosan cisitu indah permata';
        $dataKosan->foto_kosan = 'kosan_1.jpg';
        $dataKosan->harga_kosan = '25000';
        $dataKosan->tempobayar = 'perbulan';
        $dataKosan->nama_kontak = 'Pak kusmana';
        $dataKosan->alamat = 'jalan cisitu lama nomor 23 bandung';
        $dataKosan->jenis_penghuni = 'Pria/Wanita';
        $dataKosan->nomer_kontak = '08976777777';
        $dataKosan->deskripsi_kosan = 'kosan nyaman dekat dengan kosan yanglain , pemilik kosan ramah tamah';
        $dataKosan->slug_url = str_slug($dataKosan->name);
        $dataKosan->user_id = '2';
        $dataKosan->view_count = '0';
        $dataKosan->save();
    }
}
