<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // untuk Role admin Website
      $adminRole = new Role();
      $adminRole->name = 'admin';
      $adminRole->display_name = 'Admin Website';
      $adminRole->description = 'admin kelola user, kelola kosan,melihat list kosan';
      $adminRole->save();

        // untuk Role admin kosan
      $memberRole = new Role();
      $memberRole->name = 'member';
      $memberRole->display_name = 'Admin Kosan';
      $memberRole->description = 'kelola kosan , melihat list kosan';
      $memberRole->save();

        // untuk Role akun nonaktif
      $suspendRole = new Role();
      $suspendRole->name = 'suspend';
      $suspendRole->display_name = 'Akun Suspend';
      $suspendRole->description = 'Akun tidak bisa melakukan apapun di web';
      $suspendRole->save();

        // untuk membuat sample admin Website
      $admin = new User();
      $admin->first_name = 'Sigit';
      $admin->last_name = 'Nurhanafi';
      $admin->email = 'sigit.nurhanafi@gmail.com';
      $admin->password = bcrypt('rahasia');
      $admin->save();
      $admin->attachRole($adminRole);

        // untuk membuat sample admin kosan
      $admin = new User();
      $admin->first_name = 'nurhanafi';
      $admin->last_name = 'sigit';
      $admin->email = 'nurhanafi1996@gmail.com';
      $admin->password = bcrypt('rahasia');
      $admin->save();
      $admin->attachRole($memberRole);

    }
}
